# TODO prendre le login/pass depuis des var d'env ??
# TODO mettre le flatpak en local
# 
FROM fedora:29
MAINTAINER misc
RUN dnf install -y git cmake make gcc libcurl-devel gcc-c++ boost-devel openssl-devel liboauth-devel jsoncpp-devel tinyxml2-devel htmlcxx-devel rhash-devel
RUN cd /srv/ && git clone https://github.com/Sude-/lgogdownloader.git && cd /srv/lgogdownloader && cmake . && make
RUN dnf history rollback 1 -y ; dnf install -y boost liboauth jsoncpp htmlcxx rhash tinyxml2 ; dnf clean all
CMD /srv/lgogdownloader/lgogdownloader

