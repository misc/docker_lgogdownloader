#!/bin/bash
NAME=lgogdownloader
CONTAINER="localhost/$NAME"

DIRECTORY=/tmp/games

mkdir -p config games
chcon -t container_file_t config games

podman inspect $CONTAINER >/dev/null 2>/dev/null
if [ $? -gt 0 ]; then
	podman build --squash -t $NAME .
fi
podman run --rm -v $PWD/config:/root/.config -v $PWD/games:$DIRECTORY -ti $NAME /srv/lgogdownloader/lgogdownloader  --directory $DIRECTORY $*
